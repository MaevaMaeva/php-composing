<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Computer science figures</h1>
<div class="ui three stackable cards">
<?php
    
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

 

// //open the file
// $h = fopen("cs_figures.csv", "r+");

// //get the data
// $data = fgetcsv($h, 1000, ",");

// //read the date with a loop
// while (($data = fgetcsv($h, 1000, ",")) !== FALSE) 
// {
//     echo "<pre>";
//  var_dump($data);
// echo "</pre>";

// } 

use League\Csv\Reader;

require 'vendor/autoload.php';

$mesDonnees = Reader::createFromPath('cs_figures.csv', 'r');
$mesDonnees->setHeaderOffset(0); 
foreach ($mesDonnees as $valeur) {

?>

<div class="ui card">
  <div class="image">
    <img src="<?php echo $valeur['picture']; ?>">
  </div>
  <div class="content">
    <a class="header"><?php echo $valeur['name']?></a>
    <div class="description"><?php echo $valeur ['title']?>
    </div>
  </div>
  <div class="extra content">
    <a>
      <i class="user icon"></i>
     <?php echo $valeur ['role']?>
    </a>
  </div>
  <div class="meta">
      <span class="right floated">Born in<?php echo $valeur ['birthyear']?></span>
    </div>
</div>  
<?php
}
?>
</div>
</body>
</html>
    
